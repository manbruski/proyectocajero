﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using ProyectoCajero.Models;

namespace ProyectoCajero.Frames
{
    public partial class ClientLogin : Form
    {
        /// <summary>
        /// Variable de tipo string que guarda el nombre 
        /// del usuario
        /// </summary>
        private String name;
        private String rut = @"C:\Users\Frost\Desktop\ProyectoCajero\ProyectoCajero\ProyectoCajero\bin\Release\Clients.xml";
        public ClientLogin()
        {
            InitializeComponent();

        }

        private void btnAccess_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Activa la acción del boton de aceptar.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAccept_Click(object sender, EventArgs e)
        {

            Boolean flag = false;
            XmlDocument doc = new XmlDocument();
            doc.Load(rut);

            foreach (XmlNode node in doc.SelectNodes("//Clients"))
            {
                String userName = node.SelectSingleNode("ClientCode").InnerText;
                String passwork = node.SelectSingleNode("Pin").InnerText;

                while ((userName == txtUserName.Text & passwork == txtPassword.Text))
                {
                    flag = true;
                    name = txtUserName.Text;
                    this.OpenAdmin(flag);
                    return;
                }
            }
            this.OpenAdmin(flag);
        }
        
        /// <summary>
        ///Activa al form del administrador. 
        /// </summary>
        /// <param name="flag"></param>
        private void OpenAdmin(Boolean flag)
        {
            if (flag)
            {
                ClientMain um = new ClientMain(name);
                MoneyRetirement mr = new MoneyRetirement(name);
                mr.Name = name;
                um.Name = name;
                um.Show();
                MessageBox.Show("Bienvenido " + mr.Name);
                this.Hide();
            }
            else
            {
                MessageBox.Show("Usuario o contraseña inválida");
                txtUserName.Text = String.Empty;
                txtPassword.Text = String.Empty;
            }

        }
        
        /// <summary>
        ///Cuando se cierra el frame, abre el el form donde seleeciona el usuario del cliente. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClientLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            ChooseType ct = new ChooseType();
            ct.Show();
        }

        private void ClientLogin_Load(object sender, EventArgs e)
        {

        }

    }
}
