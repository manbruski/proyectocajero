﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace ProyectoCajero.Frames
{
    public partial class Password : Form
    {
        private String _name;
        private DataTable dt;
        private String FILE_NAME = @"C:\\Users\\Frost\\Desktop\\ProyectoCajero\\ProyectoCajero\\ProyectoCajero\\bin\\Release\\Clients.xml";


        public Password(string name)
        {
            InitializeComponent();

            dt = new DataTable();
            dt.TableName = "Clients";
            dt.Columns.Add("ClientCode", typeof(String));
            dt.Columns.Add("Pin", typeof(String));
            dt.Columns.Add("Balance", typeof(String));

            dt.Columns["ClientCode"].Caption = "Codigo de Usuario";
            dt.Columns["Pin"].Caption = "PIN";
            dt.Columns["Balance"].Caption = "Consulta de saldo";

            this.dgClients.DataSource = dt;// se pega al origen de datos
            this.ReadXML();
            this.dgClients.Columns[0].HeaderText = dt.Columns[0].Caption;
            this.dgClients.Columns[1].HeaderText = dt.Columns[1].Caption;
            this.dgClients.Columns[2].HeaderText = dt.Columns[2].Caption;

            this._name = name;
            

        }

        /// <summary>
        /// Método que modifica el archivo xml
        /// </summary>
        private void WriteXML()
        {
            dt.WriteXml(FILE_NAME);
        }
        /// <summary>
        /// Metodo que si el archivo XML existe, lo lee.
        /// </summary>
        private void ReadXML()
        {
            if (File.Exists(FILE_NAME))
            {
                dt.ReadXml(FILE_NAME);
            }
        }

        /// <summary>
        /// Métodos get y set de name.
        /// </summary>
        public String Name1
        {
            get { return _name; }
            set { _name = value; }
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (!(txtNewPass.Text.Equals(String.Empty)
                && txtNewPassRepeat.Text.Equals(String.Empty)))
            {
                this.ChangePin();
            }
            else
            {
                MessageBox.Show("Debe de llenar todos los campos de texto.");
            }

        }

        private void ChangePin()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(FILE_NAME);
            foreach (DataGridViewRow row in dgClients.Rows)
            {
                foreach (XmlNode node in doc.SelectNodes("//Clients"))
                {
                    if ((Convert.ToString(row.Cells["ClientCode"].Value) == this._name))
                    {
                        if (txtNewPass.Text.Equals(txtNewPassRepeat.Text))
                        {
                            row.Cells["Pin"].Value = txtNewPass.Text;
                            this.WriteXML();
                            MessageBox.Show("Cambio satisfactorio");
                            txtNewPass.Text = String.Empty;
                            txtNewPassRepeat.Text = String.Empty;
                        }
                        else
                        {
                            MessageBox.Show("Error");
                            txtNewPass.Text = String.Empty;
                            txtNewPassRepeat.Text = String.Empty;
                        }
                    }
                    return;
                }
            }
        }

    }
}
