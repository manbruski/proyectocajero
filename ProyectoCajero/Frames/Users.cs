﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using ProyectoCajero.Framework;


namespace ProyectoCajero.Frames
{
    public partial class Users : Form
    {
        private Boolean isEditing = false;
        private DataTable dt;
        private String FILE_NAME =
        @"C:\Users\Frost\Desktop\ProyectoCajero\ProyectoCajero\ProyectoCajero\bin\Release\Users.xml";

        public Users()
        {
            InitializeComponent();
            dt = new DataTable();
            dt.TableName = "Users";
            dt.Columns.Add("UserID", typeof(String));
            dt.Columns.Add("Username", typeof(String));
            dt.Columns.Add("Password", typeof(String));


            dt.Columns["UserID"].Caption = "ID";
            dt.Columns["Username"].Caption = "Usuario";
            dt.Columns["Password"].Caption = "Contraseña";

            this.dgUsers.DataSource = dt;// se pega al origen de datos
            this.ReadXML();
            this.dgUsers.Columns[0].HeaderText = dt.Columns[0].Caption;
            this.dgUsers.Columns[1].HeaderText = dt.Columns[1].Caption;
            this.dgUsers.Columns[2].HeaderText = dt.Columns[2].Caption;
            this.ResetToolbar();
            SettingControls(StateOfControls.Disable);

        }

        private void UsersForm_Load(object sender, EventArgs e) 
        {
            ClearControls();
            ResetToolbar();
            SettingControls(StateOfControls.Disable);

            dgUsers.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgUsers.MultiSelect = false;
        }

        private void ClearControls()
        {
            this.txtID.Text = string.Empty;
            this.txtUsername.Text = string.Empty;
            this.txtPassword.Text = string.Empty;
            this.txtID.Focus();
        }
        private void SettingToolbarButtons(NewButtonState newButtonState, EditButtonState editButtonState,
            SaveButtonState saveButtonState, CancelButtonState cancelButtonState,
            DeleteButtonState deleteButtonState, FindButtonState findButtonState,
            FilterButtonState filterButtonState, ReloadButtonState reloadButtonState,
            ExitButtonState exitButtonState)
        {
            this.tsbNew.Enabled = (newButtonState == NewButtonState.Enable ? true : false);
            this.tsbEdit.Enabled = (editButtonState == EditButtonState.Enable ? true : false);
            this.tsbSave.Enabled = (saveButtonState == SaveButtonState.Enable ? true : false);
            this.tsbCancel.Enabled = (cancelButtonState == CancelButtonState.Enable ? true : false);
            this.tsbDelete.Enabled = (deleteButtonState == DeleteButtonState.Enable ? true : false);
        }

        private void ResetToolbar()
        {
            this.SettingToolbarButtons(
                NewButtonState.Enable, EditButtonState.Enable,
                SaveButtonState.Disable, CancelButtonState.Disable,
                DeleteButtonState.Enable, FindButtonState.Enable,
                FilterButtonState.Disable, ReloadButtonState.Enable,
                ExitButtonState.Enable);
            

        }

        private void SettingControls(StateOfControls stateOfControls)
        {
            Boolean state = (stateOfControls == StateOfControls.Enable ? true : false);
            this.txtID.Enabled = state;
            this.txtUsername.Enabled = state;
            this.txtPassword.Enabled = state;
            if (!isEditing)
                dgUsers.ClearSelection();
            this.dgUsers.Enabled = !state;
        }

         private void LoadControls()
         {
            DataGridViewRow row = this.dgUsers.Rows[dgUsers.CurrentRow.Index];
            if (row.Cells[0].Value != null)
            {
                this.txtID.Text = row.Cells["UserID"].Value.ToString();
                this.txtUsername.Text = row.Cells["Username"].Value.ToString();
                this.txtPassword.Text = row.Cells["Password"].Value.ToString();
             
            }
            
        }

        private void WriteXML() {
            dt.WriteXml(FILE_NAME);
        }

        private void ReadXML()
        {
            if (File.Exists(FILE_NAME))
            {
                dt.ReadXml(FILE_NAME);
            }
        }

        private void tsbNew_Click(object sender, EventArgs e)
        {
            SettingControls(StateOfControls.Enable);
            ClearControls();
            this.SettingToolbarButtons(
                NewButtonState.Disable, EditButtonState.Disable,
                SaveButtonState.Enable, CancelButtonState.Enable,
                DeleteButtonState.Disable, FindButtonState.Disable,
                FilterButtonState.Disable, ReloadButtonState.Disable,
                ExitButtonState.Enable);
        }

        private void tsbExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tsbCancel_Click(object sender, EventArgs e)
        {
            this.isEditing = false;
            ClearControls();
            ResetToolbar();
            SettingControls(StateOfControls.Disable);
        }

        private void tsbSave_Click(object sender, EventArgs e)
        {

            if (isEditing)
            {
                this.dgUsers["UserID", dgUsers.CurrentRow.Index].Value = this.txtID.Text;
                this.dgUsers["Username", dgUsers.CurrentRow.Index].Value = this.txtUsername.Text;
                this.dgUsers["Password", dgUsers.CurrentRow.Index].Value = this.txtPassword.Text;

            }
            else
            {
                DataRow dr;
                dr = dt.NewRow();
                dr["UserID"] = txtID.Text;
                dr["Username"] = txtUsername.Text;
                dr["Password"] = txtPassword.Text;
                dt.Rows.Add(dr);
            }
            this.isEditing = false;
            this.ClearControls();
            this.ResetToolbar();
            SettingControls(StateOfControls.Disable);
            this.WriteXML();
        }

        private void tsbEdit_Click(object sender, EventArgs e)
        {
            if (dgUsers.Rows.Count > 0 && dgUsers.SelectedRows.Count > 0)
            {
                this.isEditing = true;
                this.LoadControls();
                SettingControls(StateOfControls.Enable);
                txtID.Focus();
                this.SettingToolbarButtons(
                    NewButtonState.Disable, EditButtonState.Disable,
                    SaveButtonState.Enable, CancelButtonState.Enable,
                    DeleteButtonState.Disable, FindButtonState.Disable,
                    FilterButtonState.Disable, ReloadButtonState.Disable,
                    ExitButtonState.Enable);
            }
            else
                MessageBox.Show("Seleccione la fila que desea editar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void tsbDelete_Click(object sender, EventArgs e)
        {
            if (dgUsers.Rows.Count > 0 && dgUsers.SelectedRows.Count > 0)
            {
                dgUsers.Rows.RemoveAt(dgUsers.CurrentRow.Index);
                this.dgUsers.Refresh();
                //RefreshDataGridView();
                if (dgUsers.Rows.Count <= 0)
                    ClearControls();
                this.ResetToolbar();
                this.WriteXML();
            }
            else
                MessageBox.Show("Seleccione la fila que desea borrar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void Users_Load(object sender, EventArgs e)
        {

        }
    }
}
