﻿namespace ProyectoCajero.Frames
{
    partial class MoneyRetirement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtBalanceDeduct = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dgClients = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgClients)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ingrese el monto a retirar";
            // 
            // txtBalanceDeduct
            // 
            this.txtBalanceDeduct.Location = new System.Drawing.Point(166, 54);
            this.txtBalanceDeduct.Name = "txtBalanceDeduct";
            this.txtBalanceDeduct.Size = new System.Drawing.Size(100, 20);
            this.txtBalanceDeduct.TabIndex = 1;
            this.txtBalanceDeduct.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBalanceDeduct_KeyPress);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(166, 80);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Aceptar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dgClients
            // 
            this.dgClients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgClients.Location = new System.Drawing.Point(76, 109);
            this.dgClients.Name = "dgClients";
            this.dgClients.Size = new System.Drawing.Size(240, 86);
            this.dgClients.TabIndex = 3;
            this.dgClients.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(314, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "label2";
            // 
            // MoneyRetirement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 199);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgClients);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtBalanceDeduct);
            this.Controls.Add(this.label1);
            this.Name = "MoneyRetirement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MoneyRetirement";
            ((System.ComponentModel.ISupportInitialize)(this.dgClients)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBalanceDeduct;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dgClients;
        private System.Windows.Forms.Label label2;
    }
}