﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace ProyectoCajero.Frames
{
    public partial class MoneyRetirement : Form
    {
        private String _name;
        private DataTable dt;
        private String FILE_NAME = @"C:\\Users\\Frost\\Desktop\\ProyectoCajero\\ProyectoCajero\\ProyectoCajero\\bin\\Release\\Clients.xml";
     
        public MoneyRetirement(string name)
        {
            InitializeComponent();

            dt = new DataTable();
            dt.TableName = "Clients";
            dt.Columns.Add("ClientCode", typeof(String));
            dt.Columns.Add("Pin", typeof(String));
            dt.Columns.Add("Balance", typeof(String));

            dt.Columns["ClientCode"].Caption = "Codigo de Usuario";
            dt.Columns["Pin"].Caption = "PIN";
            dt.Columns["Balance"].Caption = "Consulta de saldo";

            this.dgClients.DataSource = dt;// se pega al origen de datos
            this.ReadXML();
            this.dgClients.Columns[0].HeaderText = dt.Columns[0].Caption;
            this.dgClients.Columns[1].HeaderText = dt.Columns[1].Caption;
            this.dgClients.Columns[2].HeaderText = dt.Columns[2].Caption;

            this._name = name;
            label2.Text = _name;
        }

        /// <summary>
        /// Acción del boton para reducir el el saldo.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (!txtBalanceDeduct.Text.Equals(""))
            {
                this.ReduceBalance();
            }
            else 
            {
                MessageBox.Show("Debe de ingresar un valor");
            }

        }

        /// <summary>
        /// Método que impide que el usuario ingrese letras.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBalanceDeduct_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Método que se encarga de reducir saldo a una cuenta 
        /// y ela forma en la que lo hace es que pregunta
        /// </summary>
        private void ReduceBalance()
        {
            int bal = Convert.ToInt32(txtBalanceDeduct.Text);
            XmlDocument doc = new XmlDocument();
            doc.Load(FILE_NAME);
            foreach (DataGridViewRow row in dgClients.Rows)
            {
            foreach (XmlNode node in doc.SelectNodes("//Clients"))
            {
                String totalBalance = Convert.ToString(row.Cells["Balance"].Value);
                int balance = Convert.ToInt32(totalBalance);

                if ((Convert.ToString(row.Cells["ClientCode"].Value) == this._name))
                {
                    if(balance <= bal){
                        MessageBox.Show("Error");
                    }else
	
                    {
                        row.Cells["Balance"].Value = balance - bal;
                        this.WriteXML();
                        MessageBox.Show("Retire su dinero");
                        return; 
	
                    } 
                }
            }
            }
            
        }



        /// <summary>
        /// Método que modifica el archivo xml
        /// </summary>
        private void WriteXML()
        {
            dt.WriteXml(FILE_NAME);
        }
        /// <summary>
        /// Metodo que si el archivo XML existe, lo lee.
        /// </summary>
        private void ReadXML()
        {
            if (File.Exists(FILE_NAME))
            {
                dt.ReadXml(FILE_NAME);
            }
        }

        /// <summary>
        /// Métodos get y set de name.
        /// </summary>
        public String Name1
        {
            get { return _name; }
            set { _name = value; }
        }


    }
}
