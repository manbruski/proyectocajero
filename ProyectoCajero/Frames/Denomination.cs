﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using ProyectoCajero.Framework;

namespace ProyectoCajero.Frames
{
    public partial class Denomination : Form
    {
        private Boolean isEditing = false;
        private DataTable dt;
        private String FILE_NAME =
        @"C:\Users\Frost\Desktop\ProyectoCajero\ProyectoCajero\ProyectoCajero\bin\Release\Denominations.xml";

        public Denomination()

        {
            InitializeComponent();
      
            dt = new DataTable();
            dt.TableName = "Denomination";
            dt.Columns.Add("Money", typeof(String));
            dt.Columns.Add("Quantity", typeof(String));

            dt.Columns["Money"].Caption = "Billete";
            dt.Columns["Quantity"].Caption = "Cantidad";
           
            this.dgDenomination.DataSource = dt;// se pega al origen de datos
            this.ReadXML();
            this.dgDenomination.Columns[0].HeaderText = dt.Columns[0].Caption;
            this.dgDenomination.Columns[1].HeaderText = dt.Columns[1].Caption;
            this.ResetToolbar();
            SettingControls(StateOfControls.Disable);
        }

        private void UsersForm_Load(object sender, EventArgs e)
        {
            ClearControls();
            ResetToolbar();
            SettingControls(StateOfControls.Disable);

            dgDenomination.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgDenomination.MultiSelect = false;
        }

        private void ClearControls()
        {
            this.txtAmount.Text = string.Empty;
            this.txtQuantity.Text = string.Empty;
            this.txtAmount.Focus();
        }
        private void SettingToolbarButtons(NewButtonState newButtonState, EditButtonState editButtonState,
            SaveButtonState saveButtonState, CancelButtonState cancelButtonState,
            DeleteButtonState deleteButtonState, FindButtonState findButtonState,
            FilterButtonState filterButtonState, ReloadButtonState reloadButtonState,
            ExitButtonState exitButtonState)
        {
            this.tsbNew.Enabled = (newButtonState == NewButtonState.Enable ? true : false);
            this.tsbEdit.Enabled = (editButtonState == EditButtonState.Enable ? true : false);
            this.tsbSave.Enabled = (saveButtonState == SaveButtonState.Enable ? true : false);
            this.tsbCancel.Enabled = (cancelButtonState == CancelButtonState.Enable ? true : false);
            this.tsbDelete.Enabled = (deleteButtonState == DeleteButtonState.Enable ? true : false);
            this.tsbReload.Enabled = (reloadButtonState == ReloadButtonState.Enable ? true : false);

        }

        private void ResetToolbar()
        {
            this.SettingToolbarButtons(
                NewButtonState.Enable, EditButtonState.Enable,
                SaveButtonState.Disable, CancelButtonState.Disable,
                DeleteButtonState.Enable, FindButtonState.Enable,
                FilterButtonState.Disable, ReloadButtonState.Enable,
                ExitButtonState.Enable);


        }

        private void SettingControls(StateOfControls stateOfControls)
        {
            Boolean state = (stateOfControls == StateOfControls.Enable ? true : false);
            this.txtAmount.Enabled = state;
            this.txtQuantity.Enabled = state;
            if (!isEditing)
                dgDenomination.ClearSelection();
            this.dgDenomination.Enabled = !state;
        }

        private void LoadControls()
        {
            DataGridViewRow row = this.dgDenomination.Rows[dgDenomination.CurrentRow.Index];
            if (row.Cells[0].Value != null)
            {
                this.txtAmount.Text = row.Cells["Money"].Value.ToString();
                this.txtQuantity.Text = row.Cells["Quantity"].Value.ToString();

            }

        }

        private void WriteXML()
        {
            dt.WriteXml(FILE_NAME);
        }

        private void ReadXML()
        {
            if (File.Exists(FILE_NAME))
            {
                dt.ReadXml(FILE_NAME);
            }
        }

        private void tsbNew_Click(object sender, EventArgs e)
        {
            SettingControls(StateOfControls.Enable);
            ClearControls();
            this.SettingToolbarButtons(
                NewButtonState.Disable, EditButtonState.Disable,
                SaveButtonState.Enable, CancelButtonState.Enable,
                DeleteButtonState.Disable, FindButtonState.Disable,
                FilterButtonState.Disable, ReloadButtonState.Disable,
                ExitButtonState.Enable);
        }

        private void tsbCancel_Click(object sender, EventArgs e)
        {
            this.isEditing = false;
            ClearControls();
            ResetToolbar();
            SettingControls(StateOfControls.Disable); this.Close();
        }

        private void tsbSave_Click(object sender, EventArgs e)
        {


            if (isEditing)
            {
                this.dgDenomination["Money", dgDenomination.CurrentRow.Index].Value = this.txtAmount.Text;
                this.dgDenomination["Quantity", dgDenomination.CurrentRow.Index].Value = this.txtQuantity.Text;

            }
            else
            {
                DataRow dr;
                dr = dt.NewRow();
                dr["Money"] = txtAmount.Text;
                dr["Quantity"] = txtQuantity.Text;
                dt.Rows.Add(dr);
            }
            this.isEditing = false;
            this.ClearControls();
            this.ResetToolbar();
            SettingControls(StateOfControls.Disable);
            this.WriteXML();
        }

        private void tsbEdit_Click(object sender, EventArgs e)
        {
            if (dgDenomination.Rows.Count > 0 && dgDenomination.SelectedRows.Count > 0)
            {
                this.isEditing = true;
                this.LoadControls();
                SettingControls(StateOfControls.Enable);
                txtAmount.Focus();
                this.SettingToolbarButtons(
                    NewButtonState.Disable, EditButtonState.Disable,
                    SaveButtonState.Enable, CancelButtonState.Enable,
                    DeleteButtonState.Disable, FindButtonState.Disable,
                    FilterButtonState.Disable, ReloadButtonState.Disable,
                    ExitButtonState.Enable);
            }
            else
                MessageBox.Show("Seleccione la fila que desea editar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        
        }

        private void tsbDelete_Click(object sender, EventArgs e)
        {
            if (dgDenomination.Rows.Count > 0 && dgDenomination.SelectedRows.Count > 0)
            {
                dgDenomination.Rows.RemoveAt(dgDenomination.CurrentRow.Index);
                this.dgDenomination.Refresh();
                //RefreshDataGridView();
                if (dgDenomination.Rows.Count <= 0)
                    ClearControls();
                this.ResetToolbar();
                this.WriteXML();
            }
            else
                MessageBox.Show("Seleccione la fila que desea borrar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtQuantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

    }
}
