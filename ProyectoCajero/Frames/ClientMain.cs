﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectoCajero.Models;
using System.Xml;

namespace ProyectoCajero.Frames
{
    public partial class ClientMain : Form
    {
        private string _name; 
        private MoneyRetirement mr;
        private Password p;
        private MoneyTransfer mt;
        private String name;
        private String FILE_NAME = @"C:\Users\Frost\Desktop\ProyectoCajero\ProyectoCajero\ProyectoCajero\bin\Release\Clients.xml";
       
        public ClientMain(string name )
        {
            InitializeComponent();
            _name = name;
        }

        private void mantenimientoDeUsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (mr.IsDisposed == true)
                {
                    mr = new MoneyRetirement(_name);

                }
                else
                {
                    mr.Activate();
                }
            }
            catch (Exception)
            {
                mr = new MoneyRetirement(_name);
            }

            mr.MdiParent = this;
            mr.ShowInTaskbar = false;
            mr.StartPosition = FormStartPosition.CenterScreen;
            mr.Show();


        }

        
        /// <summary>
        /// Acción del boton que llamar al form que cambia el password
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
                        try
            {
                if (p.IsDisposed == true)
                {
                    p = new Password(_name);
                }
                else
                {
                    p.Activate();
                }
            }
            catch (Exception)
            {
                p = new Password(_name);
            }

            p.MdiParent = this;
            p.ShowInTaskbar = false;
            p.StartPosition = FormStartPosition.CenterScreen;
            p.Show();
        }

        /// <summary>
        /// Acción del botón que llama al la ventana para trasferir fondos.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void transferenciaDeFondosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (mt.IsDisposed == true)
                {
                    mt = new MoneyTransfer(_name);
                }
                else
                {
                    mt.Activate();
                }
            }
            catch (Exception)
            {
                mt = new MoneyTransfer(_name);
            }

            mt.MdiParent = this;
            mt.ShowInTaskbar = false;
            mt.StartPosition = FormStartPosition.CenterScreen;
            mt.Show();
        }

        
        /// <summary>
        /// Cierra sesion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cerrarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChooseType ct = new ChooseType();
            ct.Show();
            this.Hide();
        }
        /// <summary>
        /// Si se cierra el frame se abrira el seleccionar usuario o cliente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClientMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            ChooseType ct = new ChooseType();
            ct.Show();

        }

        private void ClientMain_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Método get y set de name.
        /// </summary>
        public String Name1
        {
            get { return name; }
            set { name = value; }
        }

        private void consultaDeSaldoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(FILE_NAME);

            foreach (XmlNode node in doc.SelectNodes("//Clients"))
            {
                String userCode = node.SelectSingleNode("ClientCode").InnerText;
                String totalBalance = node.SelectSingleNode("Balance").InnerText;
                int balance = Convert.ToInt32(totalBalance);

                while ((userCode == this._name))
                {

                    MessageBox.Show("Balance: " + balance);
                  
                    return;
                }
            }
        }

       
    }
    
        
}
