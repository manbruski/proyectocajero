﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using ProyectoCajero.Framework;

namespace ProyectoCajero.Frames
{
    public partial class Clientes : Form
    {
        private Boolean isEditing = false;
        private DataTable dt;
        private String FILE_NAME = @"C:\Users\Frost\Desktop\ProyectoCajero\ProyectoCajero\ProyectoCajero\bin\Release\Clients.xml";
        public Clientes()
        {
            InitializeComponent();
            dt = new DataTable();
            dt.TableName = "Clients";
            dt.Columns.Add("ClientCode", typeof(String));
            dt.Columns.Add("Pin", typeof(String));
            dt.Columns.Add("Balance", typeof(String));

            dt.Columns["ClientCode"].Caption = "Codigo de Usuario";
            dt.Columns["Pin"].Caption = "PIN";
            dt.Columns["Balance"].Caption = "Consulta de saldo";

            this.dgClients.DataSource = dt;// se pega al origen de datos
            this.ReadXML();
            this.dgClients.Columns[0].HeaderText = dt.Columns[0].Caption;
            this.dgClients.Columns[1].HeaderText = dt.Columns[1].Caption;
            this.dgClients.Columns[2].HeaderText = dt.Columns[2].Caption;

            this.ResetToolbar();
            SettingControls(StateOfControls.Disable);
        }

        private void ClientForm_Load(object sender, EventArgs e)
        {
            ClearControls();
            ResetToolbar();
            SettingControls(StateOfControls.Disable);

            dgClients.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgClients.MultiSelect = false;
        }

        /// <summary>
        /// Limpia las cajas de texto.
        /// </summary>
        private void ClearControls()
        {
            this.txtUserCode.Text = string.Empty;
            this.txtPin.Text = string.Empty;
            this.txtBalance.Text = string.Empty;
            this.txtUserCode.Focus();
        }
        
        /// <summary>
        /// Activa y desactiva los botones necesarios dependiendo de la acción.
        /// </summary>
        /// <param name="newButtonState"></param>
        /// <param name="editButtonState"></param>
        /// <param name="saveButtonState"></param>
        /// <param name="cancelButtonState"></param>
        /// <param name="deleteButtonState"></param>
        /// <param name="findButtonState"></param>
        /// <param name="filterButtonState"></param>
        /// <param name="reloadButtonState"></param>
        /// <param name="exitButtonState"></param>
        private void SettingToolbarButtons(NewButtonState newButtonState, EditButtonState editButtonState,
            SaveButtonState saveButtonState, CancelButtonState cancelButtonState,
            DeleteButtonState deleteButtonState, FindButtonState findButtonState,
            FilterButtonState filterButtonState, ReloadButtonState reloadButtonState,
            ExitButtonState exitButtonState)
        {
            this.tsbNew.Enabled = (newButtonState == NewButtonState.Enable ? true : false);
            this.tsbEdit.Enabled = (editButtonState == EditButtonState.Enable ? true : false);
            this.tsbSave.Enabled = (saveButtonState == SaveButtonState.Enable ? true : false);
            this.tsbCancel.Enabled = (cancelButtonState == CancelButtonState.Enable ? true : false);
            this.tsbDelete.Enabled = (deleteButtonState == DeleteButtonState.Enable ? true : false);

        }


        /// <summary>
        /// Asigna al botón si está enable o disable con la clase enum
        /// </summary>
        private void ResetToolbar()
        {
            this.SettingToolbarButtons(
                NewButtonState.Enable, EditButtonState.Enable,
                SaveButtonState.Disable, CancelButtonState.Disable,
                DeleteButtonState.Enable, FindButtonState.Enable,
                FilterButtonState.Disable, ReloadButtonState.Enable,
                ExitButtonState.Enable);


        }

        /// <summary>
        /// Limpia controles.
        /// </summary>
        /// <param name="stateOfControls"></param>
        private void SettingControls(StateOfControls stateOfControls)
        {
            Boolean state = (stateOfControls == StateOfControls.Enable ? true : false);
            this.txtUserCode.Enabled = state;
            this.txtPin.Enabled = state;
            this.txtBalance.Enabled = state;
            if (!isEditing)
                dgClients.ClearSelection();
            this.dgClients.Enabled = !state;
        }

        /// <summary>
        /// LLena las cajas de texto con los valores de el dt.
        /// </summary>
        private void LoadControls()
        {
            DataGridViewRow row = this.dgClients.Rows[dgClients.CurrentRow.Index];
            if (row.Cells[0].Value != null)
            {
                this.txtUserCode.Text = row.Cells["ClientCode"].Value.ToString();
                this.txtPin.Text = row.Cells["Pin"].Value.ToString();
                this.txtBalance.Text = row.Cells["Balance"].Value.ToString();

            }

        }

        /// <summary>
        /// Guarda y modifica en el archivo XML.
        /// </summary>
        private void WriteXML()
        {
            dt.WriteXml(FILE_NAME);
        }


        /// <summary>
        /// Lee los archivos XML.
        /// </summary>
        private void ReadXML()
        {
            if (File.Exists(FILE_NAME))
            {
                dt.ReadXml(FILE_NAME);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbNew_Click(object sender, EventArgs e)
        {
            SettingControls(StateOfControls.Enable);
            ClearControls();
            this.SettingToolbarButtons(
                NewButtonState.Disable, EditButtonState.Disable,
                SaveButtonState.Enable, CancelButtonState.Enable,
                DeleteButtonState.Disable, FindButtonState.Disable,
                FilterButtonState.Disable, ReloadButtonState.Disable,
                ExitButtonState.Enable);
        }

        /// <summary>
        /// Cierra las ventanas.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Llama al ClearControls, resettoolbaly y settingControls, además de colocar el editable en false.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbCancel_Click(object sender, EventArgs e)
        {
            this.isEditing = false;
            ClearControls();
            ResetToolbar();
            SettingControls(StateOfControls.Disable);
        }

        /// <summary>
        /// Acción que se encarga de guardar los cambios en el xml
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsbSave_Click(object sender, EventArgs e)
        {
            if (isEditing)
            {
                this.dgClients["ClientCode", dgClients.CurrentRow.Index].Value = this.txtUserCode.Text;
                this.dgClients["Pin", dgClients.CurrentRow.Index].Value = this.txtPin.Text;
                this.dgClients["Balance", dgClients.CurrentRow.Index].Value = this.txtBalance.Text;
            }
            else
            {
                DataRow dr;
                dr = dt.NewRow();
                dr["ClientCode"] = txtUserCode.Text;
                dr["Pin"] = txtPin.Text;
                dr["Balance"] = txtBalance.Text;
                dt.Rows.Add(dr);
            }
            this.isEditing = false;
            this.ClearControls();
            this.ResetToolbar();
            SettingControls(StateOfControls.Disable);
            this.WriteXML();
        }

        private void tsbEdit_Click(object sender, EventArgs e)
        {
            if (dgClients.Rows.Count > 0 && dgClients.SelectedRows.Count > 0)
            {
                this.isEditing = true;
                this.LoadControls();
                SettingControls(StateOfControls.Enable);
                txtUserCode.Focus();
                this.SettingToolbarButtons(
                    NewButtonState.Disable, EditButtonState.Disable,
                    SaveButtonState.Enable, CancelButtonState.Enable,
                    DeleteButtonState.Disable, FindButtonState.Disable,
                    FilterButtonState.Disable, ReloadButtonState.Disable,
                    ExitButtonState.Enable);
            }
            else
                MessageBox.Show("Seleccione la fila que desea editar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void tsbDelete_Click(object sender, EventArgs e)
        {
            if (dgClients.Rows.Count > 0 && dgClients.SelectedRows.Count > 0)
            {
                dgClients.Rows.RemoveAt(dgClients.CurrentRow.Index);
                this.dgClients.Refresh();
                //RefreshDataGridView();
                if (dgClients.Rows.Count <= 0)
                    ClearControls();
                    this.ResetToolbar();
                    this.WriteXML();

            }
            else
                MessageBox.Show("Seleccione la fila que desea borrar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        

        /// <summary>
        /// Método que se encarga de agregar saldo a una cuenta
        /// </summary>
        private void AddBalance()
        {
            int bal = Convert.ToInt32(txtBalance.Text);
            XmlDocument doc = new XmlDocument();
            doc.Load(FILE_NAME);

            foreach (XmlNode node in doc.SelectNodes("//Clients"))
            {
                String userCode = node.SelectSingleNode("ClientCode").InnerText;
                String totalBalance = node.SelectSingleNode("Balance").InnerText;
                int balance = Convert.ToInt32(totalBalance);

                while ((userCode == txtUserCode.Text))
                {

                    this.dgClients["Balance", dgClients.CurrentRow.Index].Value = balance += bal;
                    this.ClearControls();
                    this.ResetToolbar();
                    SettingControls(StateOfControls.Disable);
                    this.WriteXML();
                    MessageBox.Show("Balance: " + balance);
                    return;
                }

            }
        }

        private void txtBalance_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void Clientes_Load(object sender, EventArgs e)
        {

        }

     
    }
}

