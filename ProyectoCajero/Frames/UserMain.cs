﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoCajero.Frames
{
    public partial class UserMain : Form
    {
        private Users users;
        private Denomination denominations;
        private Clientes cl;
        


        public UserMain()
        {
            InitializeComponent();
        }

        private void administradorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (users.IsDisposed == true)
                {
                    users = new Users();
                }
                else
                {
                    users.Activate();
                }
            }
            catch (Exception)
            {
                users = new Users();
            }

            users.MdiParent = this;
            users.ShowInTaskbar = false;
            users.StartPosition = FormStartPosition.CenterScreen;
            users.Show();
        }


        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChooseType ct = new ChooseType();
            ct.Show();
            this.Hide();

        }

        private void denominacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
                        try
            {
                if (denominations.IsDisposed == true)
                {
                    denominations = new Denomination();
                }
                else
                {
                    denominations.Activate();
                }
            }
            catch (Exception)
            {
                denominations = new Denomination();
            }

            denominations.MdiParent = this;
            denominations.ShowInTaskbar = false;
            denominations.StartPosition = FormStartPosition.CenterScreen;
            denominations.Show();
        }

        private void UserMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            ChooseType ct = new ChooseType();
            ct.Show();
            
        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (cl.IsDisposed == true)
                {
                    cl = new Clientes();
                }
                else
                {
                    cl.Activate();
                }
            }
            catch (Exception)
            {
                cl = new Clientes();
            }

            cl.MdiParent = this;
            cl.ShowInTaskbar = false;
            cl.StartPosition = FormStartPosition.CenterScreen;
            cl.Show();


        }


        }
}
