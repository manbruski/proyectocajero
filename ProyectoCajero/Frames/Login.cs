﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

using System.Xml.Linq;

namespace ProyectoCajero.Frames
{
    public partial class Login : Form
    {

 
        private String FILE_NAME = Application.StartupPath + "\\Users.xml";
        private String rut = @"C:\Users\Frost\Desktop\ProyectoCajero\ProyectoCajero\ProyectoCajero\bin\Release\Users.xml";
        

        public Login()
        {

            InitializeComponent();
        }

        private void btnAccess_Click(object sender, EventArgs e)
        {
            Boolean flag = false;
            XmlDocument doc = new XmlDocument();
            doc.Load(rut);

            foreach (XmlNode node in doc.SelectNodes("//Users"))
            {
                String userName = node.SelectSingleNode("Username").InnerText;
                String passwork = node.SelectSingleNode("Password").InnerText;

                while ((userName == txtUserName.Text & passwork == txtPassword.Text))
                {
                    flag = true;
                    this.OpenAdmin(flag);
                    //flag = false;
                    return;
                }
            }
            this.OpenAdmin(flag);
            
            
        }

        private void OpenAdmin(Boolean flag) {
            if (flag)
            {
                UserMain um = new UserMain();
                um.Show();
                this.Hide();
                MessageBox.Show("Bienvenido.");
                
            }
            else
            {
                MessageBox.Show("Usuario o contraseña inválida");
                txtUserName.Text = String.Empty;
                txtPassword.Text = String.Empty;
            }
        
        }

        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            ChooseType ct = new ChooseType();
            ct.Show();
          
        }
    }
}
