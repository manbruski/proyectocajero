﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoCajero.Frames
{
    public partial class ChooseType : Form
    {
        /// <summary>
        /// Método constructor
        /// </summary>
        public ChooseType()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Boton de Administrador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdmin_Click(object sender, EventArgs e)
        {
           Login l = new Login();
            l.Show();
            this.Hide();
        }

        /// <summary>
        /// Boton de cliente
        /// Si presiona el boton de cliente entra al menu principal del cliente
        /// en donde podra preguntar por el saldo, transferir saldo y retirar saldo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClient_Click(object sender, EventArgs e)
        {
            ClientLogin cl = new ClientLogin();
            cl.Show();
            this.Hide();
        }
    }
}
