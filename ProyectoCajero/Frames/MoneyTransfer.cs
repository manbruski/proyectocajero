﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace ProyectoCajero.Frames
{
    public partial class MoneyTransfer : Form
    {
        private DataTable dt;
        private String FILE_NAME = @"C:\\Users\\Frost\\Desktop\\ProyectoCajero\\ProyectoCajero\\ProyectoCajero\\bin\\Release\\Clients.xml";

        private String _name;
    
        public MoneyTransfer(string name)
        {
            InitializeComponent();

            dt = new DataTable();
            dt.TableName = "Clients";
            dt.Columns.Add("ClientCode", typeof(String));
            dt.Columns.Add("Pin", typeof(String));
            dt.Columns.Add("Balance", typeof(String));

            dt.Columns["ClientCode"].Caption = "Codigo de Usuario";
            dt.Columns["Pin"].Caption = "PIN";
            dt.Columns["Balance"].Caption = "Consulta de saldo";

            this.dgClients.DataSource = dt;// se pega al origen de datos
            this.ReadXML();
            this.dgClients.Columns[0].HeaderText = dt.Columns[0].Caption;
            this.dgClients.Columns[1].HeaderText = dt.Columns[1].Caption;
            this.dgClients.Columns[2].HeaderText = dt.Columns[2].Caption;

            this._name = name;
            label2.Text = _name;
        }

        /// <summary>
        /// Método que se encargar de buscar al cliente al que le vamos a transferir el saldo y luego
        /// se lo suma al cliente a transferir y se lo baja al cliente actual.
        /// </summary>
        private void TransferMoney()
        {
            String cod = txtCode.Text;
            int ammount =Convert.ToInt32(txtAmmount.Text);
            XmlDocument doc = new XmlDocument();
            doc.Load(FILE_NAME);
            foreach (DataGridViewRow row in dgClients.Rows)
            {
                foreach (XmlNode node in doc.SelectNodes("//Clients"))
                {
                    String totalBalance = node.SelectSingleNode("Balance").InnerText;
                    String UserCode = node.SelectSingleNode("ClientCode").InnerText;
                    int balance = Convert.ToInt32(totalBalance);
                    
                    if (UserCode.Equals(cod))
                    {
                        if (!this.VerifyClient())
                        {
                            MessageBox.Show("Error, no posee suficientes fondos");

                        }
                         if (Convert.ToString(row.Cells["ClientCode"].Value) == txtCode.Text)
                        {

                            this.ReduceBalance();
                            row.Cells["Balance"].Value = balance + ammount;
                            this.WriteXML();
                            MessageBox.Show("Exito");
                        }
                        {
                            
                        }
                    }
                   
                }
            }
        }

        /// <summary>
        /// Método que se encarga de reducir el saldo al cliente actual.
        /// </summary>
        private void ReduceBalance()
        {
            int bal = Convert.ToInt32(txtAmmount.Text);
            XmlDocument doc = new XmlDocument();
            doc.Load(FILE_NAME);
            foreach (DataGridViewRow row in dgClients.Rows)
            {
                foreach (XmlNode node in doc.SelectNodes("//Clients"))
                {
                    String totalBalance = node.SelectSingleNode("Balance").InnerText;
                    int balance = Convert.ToInt32(totalBalance);

                    if ((Convert.ToString(row.Cells["ClientCode"].Value) == this._name))
                    {
                        if (balance <= bal)
                        {
                            MessageBox.Show("Error");
                        }
                        else
                        {
                            row.Cells["Balance"].Value = balance - bal;
                            this.WriteXML();
                            return;
                        }
                       
                        
                    }

                }
               
            }
            
            
        }




        /// <summary>
        /// Método que se encarga de verificar si el saldo ingresado del cliente, es mayor al actual en la cuenta.
        /// </summary>
        /// <returns></returns>
        private Boolean VerifyClient()
        {
            int bal = Convert.ToInt32(txtAmmount.Text);
            XmlDocument doc = new XmlDocument();
            doc.Load(FILE_NAME);
            foreach (DataGridViewRow row in dgClients.Rows)
            {
                foreach (XmlNode node in doc.SelectNodes("//Clients"))
                {
                    String totalBalance = node.SelectSingleNode("Balance").InnerText;
                    int balance = Convert.ToInt32(totalBalance);

                    if ((Convert.ToString(row.Cells["ClientCode"].Value) == this._name))
                    {
                        if (balance <= bal)
                        {
                            MessageBox.Show("Error, saldo insuficiente");
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    
                }
            }
            return false;
        }
        
        /// <summary>
        /// Método que modifica el archivo xml
        /// </summary>
        private void WriteXML()
        {
            dt.WriteXml(FILE_NAME);
        }
        /// <summary>
        /// Metodo que si el archivo XML existe, lo lee.
        /// </summary>
        private void ReadXML()
        {
            if (File.Exists(FILE_NAME))
            {
                dt.ReadXml(FILE_NAME);
            }
        }

        /// <summary>
        /// Métodos get y set de name.
        /// </summary>
        public String Name1
        {
            get { return _name; }
            set { _name = value; }
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (!(txtAmmount.Text.Equals("") && txtCode.Text.Equals("")))
            {
                this.TransferMoney();
            }
            else
            {
                MessageBox.Show("Debe de ingresar un valor");
            }

        }

        private void txtAmmount_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAmmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
