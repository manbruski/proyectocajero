﻿namespace ProyectoCajero.Frames
{
    partial class ClientMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ClientRetirement = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaDeSaldoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transferenciaDeFondosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarSesiónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ClientRetirement,
            this.salirToolStripMenuItem,
            this.consultaDeSaldoToolStripMenuItem,
            this.transferenciaDeFondosToolStripMenuItem,
            this.cerrarSesiónToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(651, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ClientRetirement
            // 
            this.ClientRetirement.Name = "ClientRetirement";
            this.ClientRetirement.Size = new System.Drawing.Size(111, 20);
            this.ClientRetirement.Text = "Retiro de efectivo";
            this.ClientRetirement.Click += new System.EventHandler(this.mantenimientoDeUsuariosToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(102, 20);
            this.salirToolStripMenuItem.Text = "Cambio de PIN ";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // consultaDeSaldoToolStripMenuItem
            // 
            this.consultaDeSaldoToolStripMenuItem.Name = "consultaDeSaldoToolStripMenuItem";
            this.consultaDeSaldoToolStripMenuItem.Size = new System.Drawing.Size(114, 20);
            this.consultaDeSaldoToolStripMenuItem.Text = "Consulta de Saldo";
            this.consultaDeSaldoToolStripMenuItem.Click += new System.EventHandler(this.consultaDeSaldoToolStripMenuItem_Click);
            // 
            // transferenciaDeFondosToolStripMenuItem
            // 
            this.transferenciaDeFondosToolStripMenuItem.Name = "transferenciaDeFondosToolStripMenuItem";
            this.transferenciaDeFondosToolStripMenuItem.Size = new System.Drawing.Size(146, 20);
            this.transferenciaDeFondosToolStripMenuItem.Text = "Transferencia de fondos";
            this.transferenciaDeFondosToolStripMenuItem.Click += new System.EventHandler(this.transferenciaDeFondosToolStripMenuItem_Click);
            // 
            // cerrarSesiónToolStripMenuItem
            // 
            this.cerrarSesiónToolStripMenuItem.Name = "cerrarSesiónToolStripMenuItem";
            this.cerrarSesiónToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.cerrarSesiónToolStripMenuItem.Text = "Cerrar Sesión";
            this.cerrarSesiónToolStripMenuItem.Click += new System.EventHandler(this.cerrarSesiónToolStripMenuItem_Click);
            // 
            // ClientMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 339);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.Name = "ClientMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cajero";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ClientMain_FormClosing);
            this.Load += new System.EventHandler(this.ClientMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ClientRetirement;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaDeSaldoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transferenciaDeFondosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarSesiónToolStripMenuItem;
    }
}