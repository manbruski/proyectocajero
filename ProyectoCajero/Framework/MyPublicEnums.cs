﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoCajero.Framework
{
    #region Type of Number Enums
    public enum TypeOfNumber
    {
        Integer,
        Double
    }
    #endregion

    #region Buttons State Enums
    /// <summary>
    /// Estado del botón nuevo
    /// </summary>
    public enum NewButtonState
    {
        Enable,
        Disable
    }
    #endregion

    #region Edit State Enums
    public enum EditButtonState
    {
        Enable,
        Disable
    }
    #endregion

    #region Save State Enums
    public enum SaveButtonState
    {
        Enable,
        Disable
    }
    #endregion

    #region Cancel State Enums
    public enum CancelButtonState
    {
        Enable,
        Disable
    }
    #endregion

    #region Delete State Enums
    public enum DeleteButtonState
    {
        Enable,
        Disable
    }
    #endregion

    #region Find State Enums
    public enum FindButtonState
    {
        Enable,
        Disable
    }
    public enum FilterButtonState
    {
        Enable,
        Disable
    }
    #endregion


    #region Exit State Enums
    public enum ExitButtonState
    {
        Enable,
        Disable
    }
    #endregion

    #region Reload State Enums
    public enum ReloadButtonState
    {
        Enable,
        Disable
    }
    #endregion

    #region Control State Enums
    public enum StateOfControls
    {
        Enable,
        Disable
    }
    #endregion
}
