﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoCajero.Models
{
    class Person
    {
        //Atributos
        private string ID;
        private string name;
        private string lastname;
        private string charter;


        public Person()
        {

        }

        public Person(string documentID, string name, string lastname, string charter)
        {
            this.ID = documentID;
            this.name = name;
            this.lastname = lastname;
            this.charter = charter;
        }

        //Properties
        public string DocumentID
        {
            get { return ID; }
            set { ID = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Lastname
        {
            get { return lastname; }
            set { lastname = value; }
        }
        public string Charter
        {
            get { return charter; }
            set { charter = value; }
        }



    }
}
