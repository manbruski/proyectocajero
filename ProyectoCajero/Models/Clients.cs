﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoCajero.Models
{

    class Clients : Person
    {
        private string password;

        public Clients(string ID, string name, string lastname, string charter, string password)
            : base(ID, name, charter, password)
        {
            this.password = password;
        }

        

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

    }
}
